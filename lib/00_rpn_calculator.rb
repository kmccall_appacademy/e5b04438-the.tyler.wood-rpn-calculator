class RPNCalculator
  attr_accessor :calculator

  def initialize
    @calculator = []
  end

  def push(arg)
    @calculator.push(arg)
  end

  def plus
    second = @calculator.pop
    first = @calculator.pop
    raise "calculator is empty" if first == nil || second == nil
    @calculator.push(first + second)
  end

  def minus
    second = @calculator.pop
    first = @calculator.pop
    raise "calculator is empty" if first == nil || second == nil
    @calculator.push(first - second)
  end

  def divide
    second = @calculator.pop
    first = @calculator.pop
    raise "calculator is empty" if first == nil || second == nil
    @calculator.push(first.to_f / second.to_f)
  end

  def times
    second = @calculator.pop
    first = @calculator.pop
    raise "calculator is empty" if first == nil || second == nil
    @calculator.push(first * second)
  end

  def value
    @calculator[-1]
  end

  def tokens(string)
    string.split(" ").map{ |x| x.to_i == 0 ? x.to_sym : x.to_i }
  end

  def evaluate(string)

    tokens(string).each do |s|
      if s == :+
        self.plus
      elsif s == :*
        self.times
      elsif s == :-
        self.minus
      elsif s == :/
        self.divide
      else
        self.push(s)
      end
    end

    self.value

  end

end
